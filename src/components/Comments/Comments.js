import React from "react";


const Comment = props => {
    const { text } = props.data;
    return (
        <>
            <p>{text === undefined ? '' : text}</p>
        </>
    )
}

export default Comment



