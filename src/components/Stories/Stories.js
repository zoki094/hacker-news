import React from "react";


import { AllNews, fetchStories } from "./../../services/data";
import "./stories.css"
import Header from "./../header/Header";
import Footer from "./../footer/Footer";
import Story from "../Story/Story";
import Spinner from "./../Spinner/Spinner";


class Stories extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      news: [],
      loadedStories: [],
    }
  }

  async componentDidMount() {
    const allNews = await AllNews()
    this.setState({ news: allNews.data })
    this.getLoadedStories()
  }

  getLoadedStories = async () => {

    const nextStories = await fetchStories(this.state.news.slice(0, 10));
    const loadedStories = this.state.loadedStories.concat(nextStories.map(story => story.data));
    const news = this.state.news.slice(10)
    this.setState({
      news,
      loadedStories: loadedStories
    })


  }


  render() {
    if (!this.state.loadedStories.length) {
      return <Spinner />
    }
    return (
      <>
        <Header />
        {this.state.loadedStories.map((story, index) => <Story  {...story} key={index} />)}
        <p className="moreStories" onClick={() => this.getLoadedStories()}>More</p>
        <Footer />
      </>
    )
  }

}

export default Stories;
