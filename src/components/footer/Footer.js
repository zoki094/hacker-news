import React from "react";
import "./footer.css"

const  Footer = () => {
    return (
        <>
        <footer>
        <div className="line"></div>
            <div className="footer">
                <ul>
                <li><a href="https://www.linkedin.com/feed/" target="_blank">Linkedin</a></li>
                <li><a href="https://www.facebook.com/" target="_blank">Facebook</a></li>
                <li><a href="/">Support</a></li>
                <li><a href="/">Security</a></li>
                <li><a href="/">Contact</a></li>
                </ul>
            </div>
            </footer>
        </>
    )
}

export default Footer;