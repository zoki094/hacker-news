import React from "react";
import "./story.css"

import { fetchComments } from "./../../services/data";
import Comments from "./../Comments/Comments";

class Story extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            comments: [],
            showComments: false,
        }
    }


    handleComments = async () => {
        if (!this.props.kids) {
            return null
        }
        const comments = await fetchComments(this.props.kids);

        this.setState(() => {
            return {
                comments,
            }
        })

    }

    isClosed = () => {
        this.setState(prevState => {
            return {
                comments: [],
                showComments: !!prevState.showComments
            }
        })
    }

    render() {
        const { title, url, by, score, descendants } = this.props;
        return (

            <div>
                <div className="story">
                    <a href={url} target="_blank"><h2 className="title">{title}</h2></a>
                    <div className="storyDetails">
                        <p>{score} points by {by}</p>
                        <p className="comments" onClick={this.handleComments}>{descendants} {!this.props.kids ? "This story has no comments" : "comments"}</p>
                    </div>
                </div>
                <div className={this.state.comments.length ? "storyComments" : ""}>
                    {this.state.comments.length > 0 ? <h2 className="exit" onClick={this.state.showComments === false ? this.isClosed : null}>Exit</h2> : null}
                    {(this.state.comments || []).map(comment => {
                        return (
                            <>
                                <Comments  {...comment} />
                            </>
                        )

                    })}
                </div>

            </div>
        )
    }
}

export default Story;