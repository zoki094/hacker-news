import React from 'react';
import './App.css';

import Stories from "./components/Stories/Stories";

function App() {
  return (
   <>
    <Stories />
   </>
  );
}

export default App;
