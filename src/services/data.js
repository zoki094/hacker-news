import axios from "axios";

const url = "https://hacker-news.firebaseio.com/v0/"

export const AllNews = () => {
    return axios.get(`${url}/topstories.json`)
}

 const GetStory = id => {
    return axios.get(`https://hacker-news.firebaseio.com/v0/item/${id}.json`)
}

export const fetchStories = (storiesIds) =>  {
    return axios.all(storiesIds.map(storyId => GetStory(storyId)))
}

export const fetchComments = (commentsId) =>  {
        return axios.all(commentsId.map(commentId => GetStory(commentId)))
    
}
